import Login from "../pages/Login";
import Register from "../pages/Register";

export const RouteNames = {
  LOGIN: "/login",
  REGISTER: "/register",
  CONTENT: "/",
};
export const publicRoutes = [
  { path: RouteNames.LOGIN, exact: true, component: Login },
  { path: RouteNames.REGISTER, exact: true, component: Register },
];

export const privateRoutes = [];
