import { Layout } from 'antd'
import React from 'react'
import RegisterForm from '../components/auth/RegisterForm'

export default function Register() {
  return (
    <Layout.Content >
      <RegisterForm/>
    </Layout.Content>
  )
}
