import { Layout } from "antd";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import AddOperationForm from "../components/operations/AddOperationForm";
import { addOperation } from "../classes/operationsFetch";

export default function AddOperation() {
  return (
    <Layout.Content>
      <AddOperationForm action={addOperation} />
    </Layout.Content>
  );
}
