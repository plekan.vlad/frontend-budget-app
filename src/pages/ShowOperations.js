import { Layout } from "antd";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import ShowOperations from "../components/operations/ShowOperations";
import { operationsFetch } from "../classes/operationsFetch";

export default function ShowOperationss() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(operationsFetch());
  }, []);
  return (
    <Layout.Content>
      <ShowOperations />
    </Layout.Content>
  );
}
