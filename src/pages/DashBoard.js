import { Layout, Spin } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getBudget, getStats } from "../classes/budgetFetch";
import { operationsFetch } from "../classes/operationsFetch";
import BudgetForm from "../components/budget/BudgetForm";
import DashboardContent from "../components/Dashboard/DashboardContent";

export default function DashBoard() {
  const { isFetching, budgetNotFound } = useSelector((state) => state.budget);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBudget());
    dispatch(getStats());
    dispatch(operationsFetch());
  }, []);
  if (isFetching) {
    return <Spin className="app__spin" tip="Loading..." />;
  }

  if (budgetNotFound) {
    return (
      <Layout.Content>
        <BudgetForm />
      </Layout.Content>
    );
  } else {
    return <DashboardContent />;
  }
}
