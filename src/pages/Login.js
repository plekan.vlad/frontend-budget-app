import { Layout } from 'antd'
import React from 'react'
import { Outlet } from 'react-router'
import LoginForm from '../components/auth/LoginForm'

export default function Login() {
  return (
    <Layout.Content >
      <LoginForm />
    </Layout.Content>
  )
}
