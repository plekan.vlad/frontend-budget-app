import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { applyMiddleware } from "redux";
import thunk from "redux-thunk";
import auth from "./AuthReducer";
import operation from "./OperationsReducer";
import budget from "./BudgetReducer";
import stats from "./BudgetStatsReducer";
import currencies from "./CurrenciesReducer";
const rootReducers = combineReducers({
  auth,
  operation,
  budget,
  stats,
  currencies,
});

const store = configureStore({ reducer: rootReducers }, applyMiddleware(thunk));

export default store;
