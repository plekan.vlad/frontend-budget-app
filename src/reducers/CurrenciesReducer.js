const defaultState = {
  currencies: {},
  isFetchingCurrencies: true,
};

export default function CurrenciesReducer(state = defaultState, action) {
  switch (action.type) {
    case "FETCH_CURRENCIES":
      return { isFetching: true };
    case "FETCH_CURRENCIES_SUCCESS":
      return {
        currencies: action.payload,
        isFetchingCurrencies: false,
      };
    case "FETCH_CURRENCIES_ERROR":
      return { currencies: {}, isFetchingCurrencies: false };
    default:
      return state;
  }
}
