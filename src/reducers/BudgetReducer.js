const defaultState = {
  budget: {},
  isFetchingBudget: true,
  budgetNotFound: true,
};

export default function budgetReducer(state = defaultState, action) {
  switch (action.type) {
    case "FETCH_BUDGET":
      return { isFetching: true };
    case "FETCH_BUDGET_SUCCESS":
      return {
        budget: action.payload,
        isFetching: false,
        budgetNotFound: false,
      };
    case "FETCH_BUDGET_ERROR":
      return { budget: {}, isFetching: false, budgetNotFound: true };
    case "DELETE_BUDGET":
      return { budget: {}, isFetching: false, budgetNotFound: true };
    default:
      return state;
  }
}
