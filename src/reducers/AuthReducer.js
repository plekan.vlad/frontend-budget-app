const defaultState = {
  isAuth: false,
  isFetching: true,
};

export default function authReducer(state = defaultState, action) {
  switch (action.type) {
    case "FETCH_LOGIN":
      return { isAuth: false, isFetching: true };
    case "FETCH_LOGIN_SUCCESS":
      return { isAuth: true, isFetching: false };
    case "FETCH_LOGIN_ERROR":
      return { isAuth: false, isFetching: false };
    default:
      return state;
  }
}
