const defaultState = {
  stats: [],
  isFetchingStats: true,
  error: "",
};

export default function budgetStatsReducer(state = defaultState, action) {
  switch (action.type) {
    case "FETCH_STATS":
      return { isFetching: true };
    case "FETCH_STATS_SUCCESS":
      return {
        stats: action.payload,
        isFetching: false,
      };
    case "FETCH_STATS_ERROR":
      return { stats: {}, isFetching: false, error: "No stats" };
    default:
      return state;
  }
}
