const defaultState = {
  operations: [],
  isFetching: true,
  isEditing: false,
};

export default function operationsReducer(state = defaultState, action) {
  switch (action.type) {
    case "FETCH_OPERATIONS":
      return { isFetching: true };
    case "FETCH_OPERATIONS_SUCCESS":
      return { operations: action.payload, isFetching: false };
    case "DELETE_OPERATION":
      return {
        operations: state.operations.filter(
          (item) => item.id !== action.payload
        ),
      };
    case "EDIT_OPERATION_CHANGE":
      return { ...state, isEditing: action.payload };
    case "LOL ":
      return { ...state };
    case "EDIT_OPERATION":
      return {
        ...state,
        operations: state.operations.map((item) => {
          if (item.id === action.payload.operation.id) {
            return action.payload.operation;
          }

          return item;
        }),
        isEditing: action.payload.isEditing,
      };
    case "FETCH_OPERATION_ERROR":
      return { operations: [], isFetching: false };
    default:
      return state;
  }
}
