import api from "../api/api";

export default class BudgetService {
  static async addBudget({ budget }) {
    console.log(budget);
    const { total_amount, keep_amount, date, currency } = budget;
    return api.post("budget/", {
      total_amount,
      keep_amount,
      start: date[0],
      end: date[1],
      currency,
    });
  }

  static async getBudget() {
    return api.get("budget/");
  }

  static async getStats() {
    return api.get("budget/stats");
  }
  static async removeBudget() {
    return api.delete("budget/");
  }

  static async getCurrencies() {
    return api.get("budget/currencies");
  }
}
