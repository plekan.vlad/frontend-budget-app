import api from "../api/api";

export default class OperataionService {
  static async addOperation({ operation }) {
    const { amount, currency, category, description, performed_at } = operation;
    console.log(operation);
    return api.post("budget/operations", {
      amount,
      currency,
      category,
      description,
      performed_at,
    });
  }

  static async fetchOperations() {
    return api.get("budget/operations");
  }

  static async deleteOperation(id) {
    return api.delete(`budget/operations/${id}`);
  }

  static async editOperation({ operation }) {
    const { amount, currency, category, description, performed_at, id } =
      operation;

    return api.put(`budget/operations/${operation.id}`, {
      amount,
      currency,
      category,
      description,
      performed_at,
      id,
    });
  }
}
