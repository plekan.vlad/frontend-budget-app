import api from "../api/api";

export default class AuthService {
  static async login(username, password) {
    return api.post("auth/login", {
      username: username,
      password: password,
    });
  }

  static async register(username, password) {
    return api.post("auth/register", {
      username: username,
      password: password,
    });
  }
}
