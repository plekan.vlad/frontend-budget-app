export function formatMoney(money, currency) {
  const moneyFormatter = new Intl.NumberFormat(navigator.language, {
    style: "currency",
    currency: currency,
  });
  return moneyFormatter.format(money);
}

export function formatMoneyInstance(moneyInstance) {
    return formatMoney(moneyInstance.amount, moneyInstance.currency)
}
