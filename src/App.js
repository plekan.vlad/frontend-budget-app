import "./App.css";
import Sider from "./components/SIder";
import AppRoutes from "./components/AppRoutes";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { checkAuth } from "./classes/authFetch";
import { getCurrencies } from "./classes/budgetFetch";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkAuth());
    dispatch(getCurrencies());
  }, []);
  return (
    <div className="app">
      <Sider />
      <AppRoutes />
    </div>
  );
}

export default App;
