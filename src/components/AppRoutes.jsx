import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router";
import Login from "../pages/Login";
import Dashboard from "../pages/DashBoard";
import Register from "../pages/Register";

import AddOperation from "../pages/AddOperation";
import ShowOperations from "../pages/ShowOperations";

export default function AppRoutes() {
  const { isAuth } = useSelector((state) => state.auth);
  return !isAuth ? (
    <Routes>
      <Route path="/login" exact="true" element={<Login />} />
      <Route path="/register" exact="true" element={<Register />} />
      <Route path="*" element={<Navigate to="/login" />} />
    </Routes>
  ) : (
    <Routes>
      <Route
        path="/budget/show_operations"
        exact="true"
        element={<ShowOperations />}
      />
      <Route
        path="/budget/add_operation"
        exact="true"
        element={<AddOperation />}
      />
      <Route path="/" exact="true" element={<Dashboard />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
}
