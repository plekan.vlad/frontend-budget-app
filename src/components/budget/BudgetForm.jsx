import { Button, DatePicker, Form, Input, Select, Spin } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addBudget } from "../../classes/budgetFetch";
export default function BudgetForm() {
  const { RangePicker } = DatePicker;
  const dispatch = useDispatch();
  const { isFetchingCurrencies, currencies } = useSelector(
    (state) => state.currencies
  );
  const [budget, setBudget] = useState({
    total_amount: "",
    keep_amount: "",
    date: "",
    currency: "",
  });
  if (isFetchingCurrencies || !currencies) {
    return <Spin tip="Loading..." />;
  }
  const handleChange = (e) => {
    setBudget((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  const selectChange = (value) => {
    setBudget((prev) => ({ ...prev, currency: value }));
  };
  const dateChange = (date, dateString) => {
    setBudget((prev) => ({ ...prev, date: dateString }));
  };
  return (
    <div className="app__form">
      <Form labelCol={{ span: 8 }} wrapperCol={{ span: 5 }} name="basic">
        <Form.Item
          label="Total amount "
          onChange={handleChange}
          value={budget.total_amount}
          rules={[{ required: true, message: "Please input total amount!" }]}
        >
          <Input name="total_amount" />
        </Form.Item>
        <Form.Item
          label="Want to keep "
          name="keep_amount"
          onChange={handleChange}
          value={budget.keep_amount}
          rules={[{ required: true, message: "Please input amount to keep!" }]}
        >
          <Input name="keep_amount" />
        </Form.Item>
        <Form.Item
          label="Currency"
          value={budget.currency}
          name="cur"
          rules={[{ required: true, message: "Please input currency!" }]}
        >
          <Select
            onSelect={selectChange}
            showSearch
            placeholder="Please select currency"
          >
            {currencies.map((item) => {
              return (
                <Select.Option key={item} value={item}>
                  {item}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item
          label="Date"
          value={budget.date}
          name="Date"
          rules={[{ required: true, message: "Please input date!" }]}
        >
          <RangePicker onChange={dateChange} />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => dispatch(addBudget({ budget }))}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
