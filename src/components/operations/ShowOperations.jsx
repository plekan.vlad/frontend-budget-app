import { Button, Card, Spin } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { operationDelete } from "../../classes/operationsFetch";
import EditOperation from "./EditOperation";

export default function ShowOperations() {
  const { operations, isFetching, isEditing } = useSelector(
    (state) => state.operation
  );
  const dispatch = useDispatch();
  const [operation, setOperation] = useState("");
  const gridStyle = {
    width: "20%",
  };
  const styleCard = {
    margin: "20px auto",
  };
  if (isFetching) {
    return <Spin className="app__spin" tip="Loading..." />;
  }
  const stopEditing = () => {
    const element = document.getElementById("sw");
    element.classList.remove("app__no-active");
    dispatch({
      type: "EDIT_OPERATION_CHANGE",
      payload: false,
    });
  };
  console.log(operations);
  if (operations) {
    return (
      <div className="app__operations">
        {isEditing ? (
          <EditOperation operation={operation} stopEditing={stopEditing} />
        ) : null}
        <div id="sw">
          {operations.map((item) => {
            const date = new Date(item.performed_at);
            return (
              <Card key={item.id} style={styleCard}>
                <Card.Grid hoverable={false} style={gridStyle}>
                  {item.description}
                </Card.Grid>
                <Card.Grid hoverable={false} style={gridStyle}>
                  {item.amount + " " + item.currency}
                </Card.Grid>
                <Card.Grid hoverable={false} style={gridStyle}>
                  {item.category}
                </Card.Grid>
                <Card.Grid hoverable={false} style={gridStyle}>
                  {date.toLocaleDateString("sv")}
                </Card.Grid>
                <Card.Grid hoverable={false} style={gridStyle}>
                  <Button
                    onClick={() => {
                      const element = document.getElementById("sw");
                      element.classList.add("app__no-active");
                      dispatch({
                        type: "EDIT_OPERATION_CHANGE",
                        payload: true,
                      });
                      setOperation(item);
                    }}
                  >
                    Edit
                  </Button>
                  <Button onClick={() => dispatch(operationDelete(item.id))}>
                    Delete
                  </Button>
                </Card.Grid>
              </Card>
            );
          })}
        </div>
      </div>
    );
  } else {
    return <div style={{ margin: " 10% 10%" }}>No operations</div>;
  }
}
