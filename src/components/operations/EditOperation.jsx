import React from "react";
import { editOperation } from "../../classes/operationsFetch";
import AddOperationForm from "./AddOperationForm";
import close from "../../img/close.png";
import { useDispatch } from "react-redux";
export default function EditOperation(props) {
  const dispatch = useDispatch();
  return (
    <div className="edit__container">
      <div className="edit__header">
        <span>Editing operation</span>
        <img
          className="edit__img"
          src={close}
          alt=""
          onClick={props.stopEditing}
        />
      </div>
      <AddOperationForm
        action={(operation) => {
          dispatch(editOperation(operation));
        }}
        operation={props.operation}
      />
    </div>
  );
}
