import { DatePicker, Form, Input, Select, Button, message, Spin } from "antd";
import TextArea from "antd/lib/input/TextArea";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
class AddOperationForm extends React.Component {
  constructor(props) {
    super(props);
    this.today = new Date().toISOString().slice(0, 10);
    this.state = {
      amount: props.operation ? props.operation.amount : "",
      currency: props.operation ? props.operation.currency : "",
      category: props.operation ? props.operation.category : "",
      description: props.operation ? props.operation.description : "",
      performed_at: props.operation
        ? props.operation.performed_at
        : moment(this.today).format(),
      id: props.operation ? props.operation.id : null,
    };
  }

  handleChange = (e) => {
    this.setState((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  selectChange = (value) => {
    this.setState((prev) => ({ ...prev, currency: value }));
  };
  dateChange = (date) => {
    this.setState((prev) => ({ ...prev, performed_at: moment(date).format() }));
  };
  success = () => {
    const element = document.getElementById("sw");
    element.classList.remove("app__no-active");
    const operation = this.state;
    this.props.operation
      ? this.props.action({ operation })
      : this.props.action({ operation })();
  };

  render() {
    const { currencies, isFetchingCurrencies } = this.props;
    if (isFetchingCurrencies || !currencies) {
      console.log("IAM HERE ,LOADING");
      return <Spin tip="Loading..." />;
    }

    return (
      <div className="app__form">
        <Form
          labelCol={{ span: 9 }}
          wrapperCol={{ span: 6 }}
          name="basic"
          onFinish={this.success}
          initialValues={{
            amount: this.state.amount,
            cur: this.state.currency,
            category: this.state.category,
            description: this.state.description,
            performed_at: moment(),
          }}
        >
          <Form.Item
            label="Amount"
            name="amount"
            onChange={this.handleChange}
            value={this.state.amount}
            rules={[{ required: true, message: "Please input amount!" }]}
          >
            <Input name="amount" />
          </Form.Item>

          <Form.Item
            label="Currency"
            value={this.state.currency}
            name="cur"
            rules={[{ required: true, message: "Please input currency!" }]}
          >
            <Select
              onSelect={this.selectChange}
              showSearch
              placeholder="Please select currency"
            >
              {currencies.map((item) => {
                return (
                  <Select.Option key={item} value={item}>
                    {item}
                  </Select.Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item
            label="Category"
            name="category"
            onChange={this.handleChange}
            value={this.state.category}
            rules={[{ required: false }]}
          >
            <Input name="category" />
          </Form.Item>
          <Form.Item
            label="Description"
            name="description"
            onChange={this.handleChange}
            value={this.state.description}
            rules={[{ required: false }]}
          >
            <TextArea name="description" rows={3} />
          </Form.Item>

          <Form.Item
            label="Date"
            value={this.state.performed_at}
            name="performed_at"
            rules={[{ required: true, message: "Please input date!" }]}
          >
            <DatePicker onChange={this.dateChange} />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 9, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = function (state) {
  return {
    currencies: state.currencies.currencies,
    isFetchingCurrencies: state.currencies.isFetchingCurrencies,
  };
};

export default connect(mapStateToProps)(AddOperationForm);
