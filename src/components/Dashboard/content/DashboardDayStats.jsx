import { Card, Spin, Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { formatMoneyInstance } from "../../../utils/money";

export default function DashboardDayStats() {
  const { stats, isFetchingStats } = useSelector((state) => state.stats);

  if (isFetchingStats) {
    return <Spin className="app__spin" tip="Loading..." />;
  }
  console.log(stats);
  var dataSource = [
    {
      key: 1,
      name: "Can spend today",
      value: formatMoneyInstance(stats.can_spend_today),
    },
    {
      key: 2,
      name: "Keeping",
      value: formatMoneyInstance(stats.currently_keeping),
    },
  ];
  if (stats.days_until_can_spend !== 0) {
    dataSource.push({
      key: 3,
      name: "Days until can spend",
      value: stats.days_until_can_spend,
    });
  }
  return (
    <Card
      className="card__dashboard"
      style={{ width: 350, height: 350 }}
      title={"Stats of the day"}
    >
      <Table
        columns={[
          {
            dataIndex: "name",
            key: "name",
          },
          {
            dataIndex: "value",
            key: "value",
            align: "right",
          },
        ]}
        dataSource={dataSource}
        showHeader={false}
        pagination={false}
      />
    </Card>
  );
}
