import { Button, Card, Spin, Table } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { removeBudget } from "../../../classes/budgetFetch";
import { formatMoney, formatMoneyInstance } from "../../../utils/money";

export default function DashboardBudget() {
  const { stats, isFetchingStats } = useSelector((state) => state.stats);
  const { budget } = useSelector((state) => state.budget);
  const dispatch = useDispatch();
  if (isFetchingStats) {
    return <Spin className="app__spin" tip="Loading..." />;
  }
  console.log(budget);
  const totalAvailable = formatMoneyInstance(stats.total_available);
  const keptWellEmoji =
    stats.currently_keeping.amount >= budget.keep_amount ? "✅" : "❌";
  const dataSource = [
    {
      key: 1,
      name: "Total available",
      value: totalAvailable,
    },
    {
      key: 2,
      name: "Days left / total",
      value: `${stats.days_left} / ${stats.total_days}`,
    },
    {
      key: 3,
      name: "Available / day",
      value: formatMoneyInstance(stats.available_per_day),
    },
    {
      key: 4,
      name: `Want to keep`,
      value: `${keptWellEmoji} ${formatMoney(
        budget.keep_amount,
        budget.currency
      )}`,
    },
  ];
  return (
    <Card
      className="card__dashboard"
      style={{ width: 350, height: 350 }}
      title={" Budget"}
      extra={
        <Button
          size="large"
          type="link"
          onClick={() => dispatch(removeBudget())}
        >
          Delete
        </Button>
      }
    >
      <Table
        columns={[
          {
            dataIndex: "name",
            key: "name",
          },
          {
            dataIndex: "value",
            key: "value",
            align: "right",
          },
        ]}
        dataSource={dataSource}
        showHeader={false}
        pagination={false}
      />
    </Card>
  );
}
