import { Button, Card, Spin, Table } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { formatMoney } from "../../../utils/money";

export default function DashboardTransaction() {
  const { operations, isFetching } = useSelector((state) => state.operation);
  const navigate = useNavigate();

  if (isFetching) {
    return <Spin className="app__spin" tip="Loading..." />;
  }
  const items = operations.slice(0, 3);
  const dataSource = items.map((item, i) => {
    return {
      key: i,
      amount: formatMoney(item.amount, item.currency),
      description: item.description,
      category: item.category,
    };
  });
  return (
    <Card
      className="card__dashboard"
      style={{ width: 450, height: 350 }}
      title={"Last transactions"}
      extra={
        <Button
          size="large"
          type="link"
          onClick={() => navigate("budget/add_operation")}
        >
          +
        </Button>
      }
    >
      <Table
        dataSource={dataSource}
        pagination={false}
        columns={[
          {
            dataIndex: "amount",
            title: "Amount",
            key: "amount",
            align: "center",
          },
          {
            dataIndex: "description",
            title: "Description",
            key: "description",
            align: "center",
          },
          {
            dataIndex: "category",
            title: "Category",
            key: "category",
            align: "center",
          },
        ]}
      />
    </Card>
  );
}
