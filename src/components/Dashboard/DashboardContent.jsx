import { Progress, Spin, Tooltip } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getStats } from "../../classes/budgetFetch";
import DashboardCard from "./DashboardCard";
import DashboardTransaction from "./content/DashboardTransaction";
import DashboardBudget from "./content/DashboardBudget";
import DashboardDayStats from "./content/DashboardDayStats";

export default function DashboardContent() {
  const { isFetchingStats, stats } = useSelector((state) => state.stats);

  if (isFetchingStats) {
    return <Spin className="app__spin" tip="Loading..." />;
  }
  if (stats) {
    return (
      <div className="dashboard__content">
        {/*
        <Tooltip title="Total available">
          <Progress
            percent={spent_percent}
            format={() =>
              `${stats.total_available.amount} ${stats.total_available.currency}`
            }
            type="circle"
          />
        </Tooltip>{" "}
          */}
        <DashboardDayStats />
        <DashboardTransaction />
        <DashboardBudget />
      </div>
    );
  }
}
