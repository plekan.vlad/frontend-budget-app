import React from "react";
import wallet from "../../img/wallet.png";

export default function DashboardCard({ info, amount }) {
  return (
    <div className="dashboard__card">
      <div className="card__img">
        <img src={wallet} alt="card_wallet" />
      </div>
      <div className="card__info">
        <span>{info}</span>
        <span>{amount}</span>
      </div>
    </div>
  );
}
