import React from 'react'
import { Button, Form, Input } from 'antd'
import { useState } from 'react'
import { registerFetch } from '../../classes/authFetch'


export default function RegisterForm() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  
  return (
    <div className='app__form'>
      <Form name="basic" 
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 8}}
            autoComplete="off">
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input value={username} 
                onChange= {(e)=> setUsername(e.target.value)} />
        </Form.Item>
        <Form.Item
          type='password'
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password value={password} 
                          onChange= {(e)=> setPassword(e.target.value)}/>
        </Form.Item>
        <Form.Item  wrapperCol={{ offset: 8, span: 16 }}>
          <Button onClick={() => registerFetch(username,password)}>
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}
