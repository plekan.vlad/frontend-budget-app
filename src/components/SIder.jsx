import { Layout, Row } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import logo from "../img/logo.png";

export default function Sider() {
  const { isAuth, isFetching } = useSelector((state) => state.auth);
  return (
    <div className="side__bar" color="#FAFAFA">
      <div className="side__header">
        <div>
          <img className="side__logo" src={logo} alt="" />
        </div>
        <span>Budgram</span>
      </div>
      <Row className="side__nav">
        {isFetching ? (
          <></>
        ) : !isAuth ? (
          <div className="side__links">
            <Link className="side__link" to="/login">
              Login
            </Link>
            <Link className="side__link" to="/register">
              Register
            </Link>
          </div>
        ) : (
          <div className="side__links">
            <Link className="side__link" to="/budget">
              Budget
            </Link>
            <Link className="side__link" to="/budget/show_operations">
              Show my operations
            </Link>
            <Link className="side__link" to="/budget/add_operation">
              Add new operation
            </Link>
            <Link
              className="side__link"
              to="/login"
              onClick={() => {
                localStorage.clear();
                window.location.reload();
              }}
            >
              Logout
            </Link>
          </div>
        )}
      </Row>
    </div>
  );
}
