import OperataionService from "../services/OperationService";
import { message } from "antd";

export const addOperation = ({ operation }) => {
  return async () => {
    try {
      const response = await OperataionService.addOperation({ operation });
      console.log("ADDIg operation", response);
      message.success("Operation was successfully added ");
    } catch (error) {
      console.log(error);
      message.error(
        `Operation was not added. Error: ${error?.response.data.detail[0].msg}`
      );
    }
  };
};

export const editOperation = ({ operation }) => {
  return async (dispatch) => {
    try {
      const response = await OperataionService.editOperation({ operation });
      dispatch({
        type: "EDIT_OPERATION",
        payload: { isEditing: false, operation: operation },
      });
      console.log(response);
      message.success("Operation was successfully changed");
    } catch (error) {
      console.log("ERR)RR", error);
      message.error(
        `Operation was not added. Error: ${error?.response.data.detail[0].msg}`
      );
    }
  };
};

export const operationsFetch = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: "FETCH_OPERATIONS" });
      const response = await OperataionService.fetchOperations();
      dispatch({ type: "FETCH_OPERATIONS_SUCCESS", payload: response.data });
      console.log(response);
    } catch (e) {
      dispatch({ type: "FETCH_OPERATIONS_ERROR" });
      console.log(e, "error operation fetch");
    }
  };
};

export const operationDelete = (id) => {
  return async (dispatch) => {
    try {
      const response = await OperataionService.deleteOperation(id);
      dispatch({ type: "DELETE_OPERATION", payload: id });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };
};
