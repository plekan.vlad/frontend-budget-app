import BudgetService from "../services/BudgetService";

export const addBudget = ({ budget }) => {
  return async (dispatch) => {
    try {
      const response = await BudgetService.addBudget({ budget });
      console.log(response, "budget adding");
      dispatch(getBudget());
      dispatch(getStats());
    } catch (error) {
      console.log(error);
    }
  };
};
export const removeBudget = () => {
  return async (dispatch) => {
    try {
      const response = await BudgetService.removeBudget();
      dispatch({ type: "DELETE_BUDGET" });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };
};

export const getBudget = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: "FETCH_BUDGET" });
      const response = await BudgetService.getBudget();
      dispatch({ type: "FETCH_BUDGET_SUCCESS", payload: response.data });
      console.log(response);
    } catch (error) {
      dispatch({ type: "FETCH_BUDGET_ERROR" });
      console.log(error, "budget");
    }
  };
};

export const getStats = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: "FETCH_STATS" });
      const response = await BudgetService.getStats();
      dispatch({ type: "FETCH_STATS_SUCCESS", payload: response.data });
      console.log(response, "stats");
    } catch (error) {
      dispatch({ type: "FETCH_STATS_ERROR" });
      console.log(error, "stats error");
    }
  };
};

export const getCurrencies = () => {
  return async (dispatch) => {
    try {
      dispatch({ type: "FETCH_CURRENCIES" });
      const response = await BudgetService.getCurrencies();
      dispatch({ type: "FETCH_CURRENCIES_SUCCESS", payload: response.data });
      console.log(response, "Currencies");
    } catch (error) {
      dispatch({ type: "FETCH_CURRENCIES_ERROR" });
      console.log(error, "Currencies error");
    }
  };
};
