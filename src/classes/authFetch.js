import AuthService from "../services/AuthService";
import api from "../api/api";
import { useNavigate } from "react-router";

export const loginFetch = (username, password) => {
  return async (dispatch) => {
    try {
      dispatch({ type: "FETCH_LOGIN" });
      const response = await AuthService.login(username, password);
      recordLoginInfo(response.data.access_token, response.data.refresh_token);
      dispatch({ type: "FETCH_LOGIN_SUCCESS" });
      console.log(response);
    } catch (e) {
      alert(e.response.data.detail);
      dispatch({ type: "FETCH_LOGIN_ERROR" });
      console.log(e.response.data, "error login");
    }
  };
};
export async function registerFetch(username, password) {
  try {
    const response = await AuthService.register(username, password);
    console.log(response, "register success");
    loginFetch(username, password);
  } catch (e) {
    alert(e.response.data.detail);
    console.log(e.response, "error register");
  }
}

export const checkAuth = () => {
  return async (dispatch) => {
    try {
      const response = await api.get("auth/check");
      dispatch({ type: "FETCH_LOGIN_SUCCESS" });
      console.log("Success check auth", response);
    } catch (e) {
      dispatch({ type: "FETCH_LOGIN_ERROR" });
      console.log(e);
    }
  };
};

export const recordLoginInfo = function (access_token, refresh_token) {
  if (access_token) {
    localStorage.setItem("access_token", access_token);
    api.defaults.headers.common["Authorization"] = "Bearer ".concat(
      access_token
    );
  } else {
    localStorage.removeItem("access_token");
    delete api.defaults.headers.common["Authorization"];
  }
  if (refresh_token) {
    localStorage.setItem("refresh_token", refresh_token);
  } else {
    localStorage.removeItem("refresh_token");
  }
};

export function performTokenRefresh(refresh_token) {
  console.log("attempting refresh token with token", refresh_token);
  return api.post("auth/refresh-token", { refresh_token });
}
