import axios from "axios";
import { recordLoginInfo, performTokenRefresh } from "../classes/authFetch";

const API_URL = process.env.REACT_APP_API_URL || "https://lazy-budget-backend.herokuapp.com/";

const api = axios.create({
  withCredentials: true,
  baseURL: API_URL,
});
api.defaults.headers.common["Authorization"] = `Bearer ${localStorage.getItem(
  "access_token"
)}`;

api.interceptors.response.use(
  (config) => {
    return config;
  },
  async (error) => {
    console.log("error?.response?.status", error?.response?.status);
    if (error?.response?.status === 401) {
      console.log("going to fix 401 in", error);
      try {
        const refresh_token = localStorage.getItem("refresh_token");
        console.log("refresh_token", refresh_token);
        if (refresh_token) {
          recordLoginInfo("", "");
          return performTokenRefresh(refresh_token)
            .then((response) => {
              console.log("token refresh response", response);
              recordLoginInfo(
                response.data.access_token,
                response.data.refresh_token
              );
              console.log(
                "success on token refresh, tokens:",
                response.data.access_token,
                response.data.refresh_token
              );
              return Promise.resolve(response);
            })
            .catch((e) => {
              console.log("error on token refresh", e.response.data);
              recordLoginInfo("", "");
              return Promise.reject(e);
            });
        } else {
          console.log("no refresh token, not trying token refresh");
          return Promise.reject(error);
        }
      } catch (e) {
        console.log("token refresh failed", e.response.data);
        return Promise.reject(error);
      }
    }
    return Promise.reject(error);
  }
);

export default api;
